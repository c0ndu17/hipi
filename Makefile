#
# Makefile
#
# Paul Reiners
# Computer Science 50
# Final Project
#


# compiler to use
CC = gcc

# flags to pass compiler
CFLAGS = -ggdb -O0 -std=gnu99 -Wall -Werror -D_GNU_SOURCE

# name for executable
EXE = hipi

# space-separated list of header files
HDRS = ./include/hipi.h ./include/gospler.h

# space-separated list of libraries, if any,
# each of which should be prefixed with -l
LIBS = -lc -lm -lgmp -lncurses -lpthread

# space-separated list of source files
SRCS = ./src/hipi.c ./src/gospler.c

# automatically generated list of object files
OBJS = $(SRCS:.c=.o)

# default target
$(EXE): $(OBJS) $(HDRS) Makefile
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LIBS)

# dependencies 
$(OBJS): $(HDRS) Makefile

# housekeeping
clean:
	rm -f $(EXE) *.o ./src/*.o
