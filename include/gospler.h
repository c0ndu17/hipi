#ifndef GOSPLER_H

#define GOSPLER_H
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* return the inverse of x mod y */
int inv_mod(int x, int y);

/* return the inverse of u mod v, if v is odd */
int inv_mod2(int u, int v);

/* return (a^b) mod m */
int pow_mod(int a, int b, int m);

/* return true if n is prime */
int is_prime(int n);

/* return the prime number immediatly after n */
int next_prime(int n);

int compute_at_position(unsigned long long int initial_position);

#endif/* GOSPLER_H */
