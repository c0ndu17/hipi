#ifndef HIPI_H

#define HIPI_H

#define _XOPEN_SOURCE
#define PLACES 1
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <gmp.h>
#include <time.h>
#include <ncurses.h>
#include <pthread.h>

#include "./gospler.h"

typedef struct WorkerArgs {
    int thread_number;
} WorkerArgsPtr;

typedef struct PiList {
    char *computed_list;
    unsigned long long int start_position;
    struct PiList *next;
} PiListPtr;

#define LIST_SIZE COLS-2

int *thread_count;
unsigned long int *array_size;
unsigned long long int current_offset;
char *screen_array;
PiListPtr *list_queue;
PiListPtr *last_in_list;
WINDOW *art_screen;
WINDOW *input_screen;
WINDOW *menu_screen;
pthread_t *thread_ptr;
pthread_mutex_t position_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t insert_lock = PTHREAD_MUTEX_INITIALIZER;;

int safe_close();
int workers_init(char *count);
int thread_starter(unsigned long long int start_val);
void *worker_proc(void *args);
int insert_list_element(PiListPtr *list_to_add);
int shift_chooser(int method_flag);
unsigned long long int compute_from_pos();

#endif
