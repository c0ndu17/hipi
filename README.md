HiPi
----

A project of mine to eventually find the words HiPi, in some form in the digits of pi.

#Current Status
---------------
Creates an ncurses TUI, and prints digits line by line, using a specified amount of threads. Once it hits the bottom it rewrites from the top down.

TODO
----
* INPUT MECHANIC
* IMPLEMENT CUDA
* INCREASE CHAR SHIFT OPTIONS
