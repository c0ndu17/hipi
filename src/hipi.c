/****************************************************************************
 * PI ART
 ***************************************************************************/


#include "../include/hipi.h"
#include "../include/gospler.h"

int safe_close() {
    delwin(art_screen);
    delwin(input_screen);
    endwin();
    if((input_screen != NULL)||(art_screen != NULL)) {
        return 0;
    } else{
        return 1;
    }
}

int workers_init(char *count) {
    thread_count = (int *)malloc(sizeof(int));
    *(thread_count) = atoi(count);
    thread_ptr = (pthread_t *) malloc(sizeof(pthread_t) * *(thread_count));
    if(*(thread_count) < 1){
        return 0;
    }
    return 1;
}

int thread_starter(unsigned long long int start_val) {
    current_offset = 1;
    for(int i = 0; i < *(thread_count); i++){
        if (pthread_create(&(thread_ptr[i]), NULL, worker_proc, NULL) < 0) {
            safe_close();
            endwin();
            printf("could not spawn worker thread(%d) for client\n", i);
            return 0;
        }
    }
    return 1;
}

void *worker_proc(void *args){
    unsigned long long int current_position;
    //WorkerArgsPtr *local_args = (WorkerArgsPtr *) local_args;
    PiListPtr *local_element;
    char *insert_list;
    int value_shift = shift_chooser(0);
    while(true) {
        current_position = compute_from_pos();
        if (current_position == -1){
            return NULL;
        }
        local_element = (PiListPtr *) malloc(sizeof(PiListPtr));
        local_element->start_position = current_position;
        local_element->computed_list = (char *)malloc(sizeof(char) * LIST_SIZE);
        insert_list = local_element->computed_list;
        for (int i = 0; i < LIST_SIZE; i++) {
            insert_list[i] = (char) compute_at_position(current_position + i) + value_shift;
        }
        //fprintf(stdout, "%s", insert_list);
        insert_list_element(local_element);
    }
    return NULL;
}
int shift_chooser(int method_flag){
    switch(method_flag){
    case 0:
        return 38;
    //case 1:
    //    break;
    //case 2:
    //    break;
    //case 3:
    //    break;
    //case 4:
    //    break;
    //case 5:
    //    break;
    //case 6:
    //    break;
    default:
        return 48;
    }
}
unsigned long long int compute_from_pos(){
    unsigned long long int new_pos;
    pthread_mutex_lock(&position_lock);
    new_pos = current_offset;
    current_offset += LIST_SIZE;
    pthread_mutex_unlock(&position_lock);
    return new_pos;
}
int insert_list_element(PiListPtr *list_to_add) {
    pthread_mutex_lock(&insert_lock);
    if(list_queue == NULL) {
        list_queue = list_to_add;
        last_in_list = list_to_add;
    } else {
        last_in_list->next = list_to_add;
        last_in_list = last_in_list->next;
    }
    pthread_mutex_unlock(&insert_lock);
    return 0;
}

int main(int argc, char **argv)
{
    char *message;
    char* usage = "Usage: ./hipi thread_count";
    if (argc == 1 || (argc == 2 && strcmp(argv[1], "-h") == 0))
    {
        printf("%s\n", usage);
        return 0;
    }
    initscr();
    echo();
    keypad(stdscr, TRUE);

    workers_init(argv[1]);
    art_screen = newwin(LINES-3, COLS, 0, 0);
    input_screen = newwin(3, COLS, LINES-3, 0);
    if((input_screen == NULL)||(art_screen == NULL)){
        printf("Failed to initialize all the screens\nExiting.\n");
        safe_close();
        return EXIT_FAILURE;
    }
    wborder(input_screen, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(input_screen);
    wborder(art_screen, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(art_screen);
    message = (char *) malloc(sizeof(char)*(COLS-2));

    mpf_set_default_prec(128);
    mp_bitcnt_t default_float_prec = mpf_get_default_prec();
    bzero(message, sizeof(char)*strlen(message));
    sprintf(message, "Default float precision is %lu bits.\n", default_float_prec);
    mvwaddnstr(input_screen, 1, 1, message, strlen(message));
    bzero(message, sizeof(char)*strlen(message));
    wrefresh(input_screen);
    
    time_t t1, t2;
    (void) time(&t1);
    int curr_pos = 1;

    array_size =  (unsigned long int *) malloc(sizeof(unsigned long int));
    *(array_size) = (LINES-5)*(COLS-2);
    screen_array = (char *) malloc(sizeof(char) * *(array_size));

    werase(input_screen);
    bzero(message, sizeof(char)*strlen(message));
    sprintf(message, "Spawning %d Threads from the pits of hell, &*&", *(thread_count));
    mvwaddnstr(input_screen, 1, 1, message, strlen(message));
    bzero(message, sizeof(char)*strlen(message));
    wborder(input_screen, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(input_screen);

    thread_starter(curr_pos);
    werase(input_screen);
    bzero(message, sizeof(char)*strlen(message));
    sprintf(message, "Threadurinos succesfully spawned");
    mvwaddnstr(input_screen, 1, 1, message, strlen(message));
    wborder(input_screen, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(input_screen);
    sleep(2);

    werase(input_screen);
    bzero(message, sizeof(char)*(COLS-2));
    sprintf(message, "COMPUTING DIGITS");
    mvwaddnstr(input_screen, 1, 1, message, (COLS-2));
    //wborder(input_screen, 0, 0, 0, 0, 0, 0, 0, 0);
    wrefresh(input_screen);

    int cursor_position_x = 1;
    int cursor_position_y = 1;
    do {
        while(list_queue != NULL){
            PiListPtr *to_free;
            to_free = list_queue;
            for(int i = 0; i< LIST_SIZE; i++, cursor_position_x++) {
                if (cursor_position_x > COLS-2) {
                    cursor_position_x = 1; 
                    cursor_position_y++;
                }
                if (cursor_position_y > LINES-5) {
                    cursor_position_y = 1; 
                }
                mvwaddch(art_screen, cursor_position_y, cursor_position_x, list_queue->computed_list[i]);
            }
            list_queue = list_queue->next;
            free(to_free->computed_list);
            free(to_free);
        }
        (void) time(&t2);
    } while((int)(t2 - t1) < 30);
    free(message);
    free(thread_ptr);
    free(thread_count);
    free(array_size);
    free(screen_array);
    safe_close();
    printf("GoodPi!\n");
    return EXIT_SUCCESS;
}

